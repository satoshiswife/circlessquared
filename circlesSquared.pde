// This code originally from Daniel Shiffman's FlowField tutorial:
// https://github.com/CodingTrain/website/blob/master/CodingChallenges/CC_024_PerlinNoiseFlowField/Processing/CC_024_PerlinNoiseFlowField/CC_024_PerlinNoiseFlowField.pde
//
// Most of this code has been greatly altered from it's source

import processing.pdf.*;

color palettes[][] = {
  { #594F4F, #547980, #45ADA8, #9DE0AD, #E5FCC2 }, 
  { #343838, #005F6B, #008C9E, #00B4CC, #00DFFC }, 
  { #413E4A, #73626E, #B38184, #F0B49E, #F7E4BE }, 
  { #F8B195, #F67280, #C06C84, #6C5B7B, #355C7D }, 
  { #3A111C, #574951, #83988E, #BCDEA5, #E6F9BC }, 
  { #A1DBB2, #F45D4C, #FACA66, #F7A541, #FEE5AD }, 
  { #5E9FA3, #DCD1B4, #FAB87F, #F87E7B, #B05574 }
};

String fileName = "_renderV8_palette_";

void setup() {
  size(2000, 2000, P2D);
}

void draw() {

  // move to the canvas
  translate(width, height);
  rotate(radians(180));

  // loop through all the color palletes
  for (int pal=0; pal < palettes.length; pal++) {

    println("palette: "+(pal));
    
    // clear the background on each new pallete
    background(255);

    // generate the flowfield
    FlowField flowfield;
    flowfield = new FlowField(200);

    // create some particles, in an array particles
    ArrayList<Particle> particles;
    particles = new ArrayList<Particle>();

    // we'll add 1000 particles to each loop
    for (int i = 0; i < 1000; i++) {
      PVector start = new PVector(i % width, i % height);
      particles.add(new Particle(start, 6));
    }

    int loopcount = 450; // 450;

    for (int col=0; col < palettes[pal].length; col++) {
      
      //  loop through each colour in each pallete, layering the colours
      beginRecord(PDF, fileName + pal +"_layer"+ col +"_#"+hex(palettes[pal][col], 6)+".pdf"); 
      println("layer: "+(col));

      // drop some particles onto our flowfield
      // use a differed noise seed each some for max randomness! 
      int noiseseed = ceil(random(1000)); 
      for (int i=0; i<loopcount; i++) {
        flowfield.update(noiseseed);
        for (Particle p : particles) {
          p.follow(flowfield);
          p.run(palettes[pal][col]);
        }
      }
      
      // add some paint splats
      makeSplat(50, palettes[pal][col]);
      loopcount -= 75; //75;
      endRecord();
    }

    println("finished palette.");
    saveFrame( fileName + pal +"_low_res.png");
  }

  noLoop();
}
