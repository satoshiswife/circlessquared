// This code taken from Daniel Shiffman's FlowField tutorial:
// https://github.com/CodingTrain/website/blob/master/CodingChallenges/CC_024_PerlinNoiseFlowField/Processing/CC_024_PerlinNoiseFlowField/Particle.pde
//
// Altered code is noted

public class Particle {
  PVector pos;
  PVector vel;
  PVector acc;
  PVector previousPos;
  float maxSpeed;

  Particle(PVector start, float maxspeed) {
    maxSpeed = maxspeed;
    pos = start;
    vel = new PVector(0, 0);
    acc = new PVector(0, 0);
    previousPos = pos.copy();
  }

  /* my edits */
  // I added a color parameter
  void run(color col) {
    update();
    edges();
    show(col);
  }
  /* end */

  void update() {
    pos.add(vel);
    vel.limit(maxSpeed);
    vel.add(acc);
    acc.mult(0);
  }

  void applyForce(PVector force) {
    acc.add(force);
  }

  /* my edits */
  // I added a color parameter
  void show(color col) {

    // I randomized the stroke weight
    strokeWeight(random(0, 3));
    stroke(col);

    line(pos.x, pos.y, previousPos.x, previousPos.y);
    updatePreviousPos();
  }
  /* end */


  void edges() {

    if (pos.x > width) {
      pos.x = 0;
      updatePreviousPos();
    }

    if (pos.x < 0) {
      pos.x = width;    
      updatePreviousPos();
    }

    if (pos.y > height) {
      pos.y = 0;
      updatePreviousPos();
    }

    if (pos.y < 0) {
      pos.y = height;
      updatePreviousPos();
    }
  }

  void updatePreviousPos() {
    this.previousPos.x = pos.x;
    this.previousPos.y = pos.y;
  }

  void follow(FlowField flowfield) {
    int x = floor(pos.x / flowfield.scl);
    int y = floor(pos.y / flowfield.scl);
    int index = x + y * flowfield.cols;
    PVector force = flowfield.vectors[index];
    applyForce(force);
  }
}
