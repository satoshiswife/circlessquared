// This is a fork from an OpenProcessing sketch
// 
// My fork: https://www.openprocessing.org/sketch/945668


void makeSplat(int loops, color fillcol) {

  for (int i = 0; i < loops; i++) {

    float zoff = 0.;
    pushMatrix();
    translate(random(150, (width-150)), random(150, (height-150)));
    fill(fillcol);
    noStroke();
    int dropNum = ceil(random(800, 1000));

    for (int k = 0; k < dropNum; k++) {

      float diameter = pow(random(1), 20);
      float distance = sq((1.0 - pow(diameter, 2)) * random(1));
      float scaledDiameter = map(diameter, 0, 1, 1, 13);
      float scaledDistance = map(distance, 0, 1, 0, width*0.3);
      float radian = random(TWO_PI);
      zoff += 0.5;
      pushMatrix();
      translate(scaledDistance * cos(radian), scaledDistance * sin(radian));
      beginShape();

      for (float a=0; a <TWO_PI; a+= 0.01) {

        float xoff = map(cos(a), -1, 1, 0, 1);
        float yoff = map(sin(a), -1, 1, 0, 1);
        float r = map(noise(xoff, yoff, zoff), 0, 1, 1, scaledDiameter);
        float x = r*cos(a);
        float y = r*sin(a);
        vertex(x, y);
      }

      endShape(CLOSE);
      popMatrix();
    }

    popMatrix();
  }
}
