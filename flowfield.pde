// This code originally from Daniel Shiffman's FlowField tutorial:
// https://github.com/CodingTrain/website/blob/master/CodingChallenges/CC_024_PerlinNoiseFlowField/Processing/CC_024_PerlinNoiseFlowField/flowfield.pde
//
// Most of this code has been greatly altered from it's source

// create a flowfield class and give it some parameters
public class FlowField {
  PVector[] vectors;
  int cols, rows;
  float inc = 0.1;
  float zoff = 0;
  int scl;

  // initalize the flowfield, using a resolution as an argument
  FlowField(int res) {
    scl = res;
    cols = floor(width / res)+1;
    rows = floor(height / res)+1;
    vectors = new PVector[cols * rows];
  }

  // function to update the flowfield and the vectors within
  void update(int seed) {
    float xoff = 0;
    for (int y = 0; y < rows; y++) { 
      float yoff = 0;
      for (int x = 0; x < cols; x++) {
        noiseSeed(seed);
        
        // this is the main formula that generates my spiral/ring patterns
        float angle = noise(xoff, yoff, zoff)  * pow(TWO_PI,TWO_PI);
        
        PVector v = PVector.fromAngle(angle);
        v.setMag(10);
        int index = x + y * cols;
        vectors[index] = v;
        xoff += inc;
      }
      yoff += inc;
    }
    zoff += 0.0004;
  }
}
